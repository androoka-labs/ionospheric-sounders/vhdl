

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

-- phase and sin/cos lengths need to be the same

entity nco is
  port (
    clk : in std_logic;
    phase : in unsigned;
    sin,cos : out signed
  );
end;

architecture behav of nco is

  -- the increase accounts for bit growth in the calculations
  constant bitgrowth : integer := integer(floor(log(2.0,real(phase'length))))+1;

  constant ITERATIONS : integer := phase'length;
  constant SIZE : integer := phase'length + bitgrowth;

  type signed_a is array (natural range <>) of signed(SIZE-1 downto 0);
  signal x, y, z : signed_a(0 to ITERATIONS);

  type quadrant is array(natural range <>) of std_logic_vector(1 downto 0);  
  signal q : quadrant(0 to ITERATIONS);
  
  function gen_atan_table(size : positive; ITERATIONS : positive) return signed_a is
    variable table : signed_a(0 to ITERATIONS-1);
  begin
    for i in table'range loop
      table(i) := to_signed(integer(arctan(2.0**(-i)) * 2.0**size / MATH_2_PI), size);
    end loop;
    return table;
  end function;

  constant ATAN_TABLE : signed_a(1 to ITERATIONS) := gen_atan_table(SIZE, ITERATIONS);

begin

  -- cordic has a gain factor, hence x(0) is different.
  x(0) <=  to_signed(integer(0.303626*(2.0**SIZE)),SIZE);
  y(0) <= (others => '0');
  z(0) <= signed("00" & phase(phase'high-2 downto 0) & to_unsigned(0,bitgrowth));
  q(0) <= std_logic_vector(phase(phase'high downto phase'high-1));


  calci : for i in 1 to ITERATIONS generate

    cordic: process(clk) is
      variable negative : boolean;
    begin
      if rising_edge(clk) then
        negative := z(i-1)(SIZE-1) = '1';
        --if z_array(i-1)(z'high) = '1' then -- z is negative
        if negative then
          x(i) <= x(i-1) + (y(i-1) / 2**(i-1));
          y(i) <= y(i-1) - (x(i-1) / 2**(i-1));
          z(i) <= z(i-1) + ATAN_TABLE(i);
        else -- z or y is positive
          x(i) <= x(i-1) - (y(i-1) / 2**(i-1));
          y(i) <= y(i-1) + (x(i-1) / 2**(i-1));
          z(i) <= z(i-1) - ATAN_TABLE(i);
        end if;
        q(i) <= q(i-1); -- pipeline the quadrant
      end if;
    end process;
  end generate;

  process(clk)
  begin
    if rising_edge(clk) then
      case q(ITERATIONS) is
      when "00" =>
        cos <= x(ITERATIONS)(SIZE-1 downto bitgrowth);
        sin <= y(ITERATIONS)(SIZE-1 downto bitgrowth);
      when "01" =>
        cos <= -y(ITERATIONS)(SIZE-1 downto bitgrowth);
        sin <= x(ITERATIONS)(SIZE-1 downto bitgrowth);
      when "10" =>
        cos <= -x(ITERATIONS)(SIZE-1 downto bitgrowth);
        sin <= -y(ITERATIONS)(SIZE-1 downto bitgrowth);
      when "11" =>
        cos <= y(ITERATIONS)(SIZE-1 downto bitgrowth);
        sin <= -x(ITERATIONS)(SIZE-1 downto bitgrowth);
      when others =>
      end case;
    end if;
  end process;

end;


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity nco_tb is
end;

architecture behav of nco_tb is

  component nco is
    port (
      clk : in std_logic;
      phase : in unsigned;
      sin,cos : out signed
    );
  end component;

  signal clk : std_logic;
  signal phase : unsigned(11 downto 0) := (others => '0');
  signal sin,cos : signed(phase'range);

  
begin


  dut : nco
    port map (
      clk => clk,
      phase => phase,
      sin => sin,
      cos => cos
    );

  
  process
  begin
    clk <= '0'; wait for 5 ns;
    clk <= '1'; wait for 5 ns;
  end process;

  process(clk)
  begin
    if rising_edge(clk) then
      phase <= phase + x"1";
    end if;
  end process;


end;